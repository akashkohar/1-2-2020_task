﻿
        using System;

namespace TypeConversion
    {
        class Conversions
        {
            static void Main(string[] args)
            {
                char charInput= 'a';
                int integerInput = charInput;
                long longInput = charInput;
                long longVar = integerInput;
                char charVar = charInput;
                float floatvariable = longInput;
                float floatInput = charVar;
                double doubleInput = floatInput;
                Console.WriteLine("Type conversion");
                Console.WriteLine("Implicit type conversions");
                Console.WriteLine("Value of char is " + charInput);
                Console.WriteLine("value of int is" + integerInput);
                Console.WriteLine("value of long is" + longInput);
                Console.WriteLine("value of float is" + floatvariable);
                Console.WriteLine("value of double is" + doubleInput);
                Console.WriteLine("value of long is" + longVar);
                Console.WriteLine("value of float is" + floatInput);


                Console.WriteLine("\nexplicit type conversions");

                double doubleVariable = 263177.56523;
                float floatNumber = (float)doubleVariable;
                long longNumber = (long)doubleVariable;
                int intNumber = (int)doubleVariable;
                Console.WriteLine("double=" + doubleVariable);
                Console.WriteLine("float=" + floatNumber);
                Console.WriteLine("long=" + longNumber);
                Console.WriteLine("int=" + intNumber);

                Console.WriteLine("\nother conversions");
                Console.WriteLine("Flaot to string=" + Convert.ToString(floatNumber));
                Console.WriteLine("int to double= " + Convert.ToDouble(intNumber));
                Console.WriteLine("double to int 32=" + Convert.ToInt32(doubleVariable));
                Console.WriteLine("float to uint32=" + Convert.ToUInt32(floatNumber));
                Console.ReadKey();


            }
        }
    }


