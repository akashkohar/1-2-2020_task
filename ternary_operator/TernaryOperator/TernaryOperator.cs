﻿using System;

namespace TernaryOperator
{
    class Colours
    {
        static void Main(string[] args)
        {
            string checkColour , resColour;
            Console.WriteLine("Enter a Colour");
            checkColour=Console.ReadLine();
            resColour = checkColour=="red" ? "red" :(checkColour=="green"?"green":(checkColour=="red"?"red":"default colour"));
            Console.WriteLine(resColour);
            
        }
    }
}